CloudHub API Wrapper
====================

An initial attempt at an implementation of the MuleSoft CloudHub API in Python.

See `Official API Documentation <http://www.mulesoft.org/documentation/display/current/API>`_.
