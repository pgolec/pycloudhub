"""
Unit tests for the functionality related to CloudHub logs
"""
# import pytest
# import httpretty

from cloudhub import LogSearchCriteria


def test_empty_log_search_criteria():
    sc = LogSearchCriteria()
    for key in sc.__dict__.keys():
        assert getattr(sc, key) is None


# @pytest.mark.parametrize('deployment_id, start_date_utc, end_date_utc, text, u'
# def test_log_search_criteria():
#     sc = LogSearchCriteria()
#     for key in sc.__dict__.keys():
#         assert getattr(sc, key) is None
