"""
Tests the functionality of the maven module.
"""
import tempfile
import os
from cloudhub.maven import *


def test_get_pom_version():
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as f:
        f.write(TEST_POM.strip())
        pom_name = f.name

    try:
        pom_version = get_pom_version(pom_name)
        assert pom_version == '2.0.11'
    finally:
        os.unlink(pom_name)


def test_get_pom_name():
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as f:
        f.write(TEST_POM.strip())
        pom_name = f.name

    try:
        name = get_pom_name(pom_name)
        assert name == 'test-artifact-id'
    finally:
        os.unlink(pom_name)


def test_get_target_file_name():
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as f:
        f.write(TEST_POM.strip())
        pom_name = f.name

    try:
        target_name = get_target_file_name(pom_name)
        assert target_name == 'test-artifact-id-2.0.11.zip'
    finally:
        os.unlink(pom_name)


# noinspection SpellCheckingInspection
TEST_POM = """
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>com.test</groupId>
    <artifactId>test-artifact-id</artifactId>
    <version>2.0.11</version>
    <packaging>mule</packaging>
    <name>Test Maven Project</name>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <mule.version>3.7.3</mule.version>
        <mule.tools.version>1.0</mule.tools.version>
        <apikit.version>1.7.1</apikit.version>
    </properties>
</project>
"""
