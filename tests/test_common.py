"""
Unit tests for the cloudhub.common module
"""
import pytest
from datetime import datetime, date
from cloudhub.common import (
    CloudHubError,
    CloudHubApiError,
    CloudHubUnauthorizedError,
    CloudHubNotFoundError,
    CloudHubApi,
    datetime_to_cloudhub_format
)


def test_throwing_cloud_hub_error():
    msg = 'Testing It'
    with pytest.raises(CloudHubError) as err:
        raise CloudHubError(msg)

    assert str(err.value) == msg


def test_throwing_cloud_hub_api_error_no_status_appended():
    msg = 'Testing It'
    status = 555
    with pytest.raises(CloudHubApiError) as err:
        raise CloudHubApiError(msg, status, append_status_code=False)

    assert str(err.value) == msg
    assert err.value.status_code == status


def test_throwing_cloud_hub_api_error_status_appended():
    msg = 'Testing It'
    status = 555
    with pytest.raises(CloudHubApiError) as err:
        raise CloudHubApiError(msg, status)

    assert str(err.value) == msg + ' (Status Code: 555)'
    assert err.value.status_code == status


def test_throwing_cloud_hub_unauthorized_error_with_explicit_args_no_status_appended():
    msg = 'Testing It'
    status = 555
    with pytest.raises(CloudHubUnauthorizedError) as err:
        raise CloudHubUnauthorizedError(msg, status, append_status_code=False)

    assert str(err.value) == msg
    assert err.value.status_code == status


def test_throwing_cloud_hub_unauthorized_error_with_explicit_args_status_appended():
    msg = 'Testing It'
    status = 555
    with pytest.raises(CloudHubUnauthorizedError) as err:
        raise CloudHubUnauthorizedError(msg, status)

    assert str(err.value) == msg + ' (Status Code: 555)'
    assert err.value.status_code == status


def test_throwing_cloud_hub_unauthorized_error_with_implicit_args():
    with pytest.raises(CloudHubUnauthorizedError) as err:
        raise CloudHubUnauthorizedError()

    assert str(err.value) == CloudHubUnauthorizedError.DEFAULT_MESSAGE + ' (Status Code: 401)'
    assert err.value.status_code == 401


@pytest.mark.parametrize('message_template, expected_message', [
        ('Testing It', 'Testing It'),
        ('{0} with id {1} not found', 'TestEntity with id 100 not found'),
        (' ', 'TestEntity ID [100] not found'),
        (None, 'TestEntity ID [100] not found'),
        (' \n\t\n  ', 'TestEntity ID [100] not found'),
    ])
def test_throwing_cloud_hub_not_found_error_with_explicit_args_no_status_appended(message_template, expected_message):
    with pytest.raises(CloudHubNotFoundError) as err:
        raise CloudHubNotFoundError(100, message=message_template, entity_name='TestEntity', append_status_code=False)

    assert str(err.value) == expected_message
    assert err.value.status_code == 400


@pytest.mark.parametrize('message_template, expected_message', [
        ('Testing It', 'Testing It (Status Code: 400)'),
        ('{0} with id {1} not found', 'TestEntity with id 100 not found (Status Code: 400)'),
        (' ', 'TestEntity ID [100] not found (Status Code: 400)'),
        (None, 'TestEntity ID [100] not found (Status Code: 400)'),
        (' \n\t\n  ', 'TestEntity ID [100] not found (Status Code: 400)'),
    ])
def test_throwing_cloud_hub_not_found_error_with_explicit_args_status_appended(message_template, expected_message):
    with pytest.raises(CloudHubNotFoundError) as err:
        raise CloudHubNotFoundError(100, message=message_template, entity_name='TestEntity')

    assert str(err.value) == expected_message
    assert err.value.status_code == 400


@pytest.mark.parametrize('dt, expected_string', [
        (datetime(2016, 1, 12, 14, 25, 19, 456782), '2016-01-12T14:25:19.457Z'),
        (datetime(2016, 1, 12, 14, 25, 19, 456382), '2016-01-12T14:25:19.456Z'),
        (datetime(2016, 1, 12, 0, 0, 0, 0), '2016-01-12T00:00:00.000Z'),
        (datetime(2016, 1, 12, 0, 0, 0, 1), '2016-01-12T00:00:00.000Z'),
        (datetime(2016, 1, 12, 0, 0, 0, 1000), '2016-01-12T00:00:00.001Z'),
        (date(2016, 1, 12), '2016-01-12T00:00:00.000Z'),
    ])
def test_datetime_to_cloudhub_format(dt, expected_string):
    actual = datetime_to_cloudhub_format(dt)
    assert actual == expected_string

EXPECTED_API_BASE_URL = CloudHubApi.API_BASE_URL


@pytest.mark.parametrize('url, expected_url', [
        ('https://cloudhub.io/api/test', 'https://cloudhub.io/api/test'),
        ('https://cloudhub.io/api/test/', 'https://cloudhub.io/api/test'),
        (' ', EXPECTED_API_BASE_URL),
        (None, EXPECTED_API_BASE_URL),
        (' \n\t\n  ', EXPECTED_API_BASE_URL),
    ])
def test_cloudhub_api_constructor(url, expected_url):
    api = CloudHubApi('a', 'b', url)
    assert api.api_base_url == expected_url
    # noinspection PyProtectedMember
    assert api._base_url == expected_url


@pytest.mark.parametrize('base_url, path, expected_path', [
        ('https://base', 'path', 'https://base/path'),
        ('https://base', '/path', 'https://base/path'),
        ('https://base', '/path/', 'https://base/path'),
        ('https://base', 'path/', 'https://base/path'),
        ('https://base/', 'path', 'https://base/path'),
        ('https://base/', '/path', 'https://base/path'),
        ('https://base/', '/path/', 'https://base/path'),
        ('https://base/', 'path/', 'https://base/path'),
        ('https://base/', '', 'https://base'),
        ('https://base/', '  ', 'https://base'),
        ('https://base/', None, 'https://base'),
        ('https://base/', '  \n\t  \n\n \t\t ', 'https://base'),
    ])
def test_cloudhub_api_combine_path(base_url, path, expected_path):
    # noinspection PyProtectedMember
    result = CloudHubApi._combine_path(base_url, path)
    assert result == expected_path


@pytest.mark.parametrize('path, expected_path', [
        ('path', CloudHubApi.API_BASE_URL + '/path'),
        ('/path', CloudHubApi.API_BASE_URL + '/path'),
        ('/path/', CloudHubApi.API_BASE_URL + '/path'),
        ('path/', CloudHubApi.API_BASE_URL + '/path'),
        ('path', CloudHubApi.API_BASE_URL + '/path'),
        ('/path', CloudHubApi.API_BASE_URL + '/path'),
        ('/path/', CloudHubApi.API_BASE_URL + '/path'),
        ('path/', CloudHubApi.API_BASE_URL + '/path'),
        ('', CloudHubApi.API_BASE_URL),
        ('  ', CloudHubApi.API_BASE_URL),
        (None, CloudHubApi.API_BASE_URL),
        ('  \n\t  \n\n \t\t ', CloudHubApi.API_BASE_URL),
    ])
def test_cloudhub_api_make_url(path, expected_path):
    api = CloudHubApi('a', 'b')
    # noinspection PyProtectedMember
    result = api._make_url(path)
    assert result == expected_path


