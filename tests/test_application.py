import zipfile
from io import BytesIO

import pytest
import httpretty

from cloudhub import (
    CloudHub,
    CloudHubApplicationTracker
)
from cloudhub.legacy import ApplicationNotFoundError, CloudHubApp


class _AppTestData(object):
    def __init__(self, name, user_name, password):
        self.name = name
        self.user_name = user_name
        self.password = password


@pytest.fixture
def app_test_data():
    return _AppTestData('test_app', 'test_user_name', 'test_password')


# noinspection PyShadowingNames
@pytest.fixture
def cloudhub_app(app_test_data):
    return CloudHubApp(app_test_data.app_name, app_test_data.user_name, app_test_data.password)


# noinspection PyShadowingNames
def test_application_tracking(app_test_data):
    api = CloudHub(app_test_data.user_name, app_test_data.password)
    tracking_api = api.application(app_test_data.name).tracking
    assert tracking_api
    assert tracking_api.user_name == app_test_data.user_name
    assert tracking_api.password == app_test_data.password
    assert tracking_api.application_name == app_test_data.name
    # noinspection PyProtectedMember
    assert tracking_api._base_url == '{0}/applications/{1}/tracking'.format(
        CloudHubApplicationTracker.API_BASE_URL,
        app_test_data.name
    )


# noinspection PyShadowingNames
class TestCloudHubAppConstructor(object):
    """
    Basic tests verifying that the CloudHubApp is being
    constructed properly.
    """

    @staticmethod
    def __create_cloud_hub_app(app_test_data):
        return CloudHubApp(app_test_data.name, app_test_data.user_name, app_test_data.password)

    def test_app_name_is_set(self, app_test_data):
        app = self.__create_cloud_hub_app(app_test_data)
        assert app_test_data.name == app.name

    def test_app_name_must_not_be_none(self, app_test_data):
        app_test_data.name = None
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_app_name_must_not_be_empty(self, app_test_data):
        app_test_data.name = ''
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_app_name_must_not_be_all_spaces(self, app_test_data):
        app_test_data.name = '    '
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_app_name_must_not_be_whitespace(self, app_test_data):
        app_test_data.name = ' \n \t  '
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_user_name_is_set(self, app_test_data):
        app_test_data.user_name = 'test_name'
        app = self.__create_cloud_hub_app(app_test_data)
        assert app_test_data.user_name == app.user_name

    def test_user_name_must_not_be_none(self, app_test_data):
        app_test_data.user_name = None
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_user_name_must_not_be_empty(self, app_test_data):
        app_test_data.user_name = ''
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_user_name_must_not_be_all_spaces(self, app_test_data):
        app_test_data.user_name = None
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_user_name_must_not_be_whitespace(self, app_test_data):
        app_test_data.user_name = ' \n \t  '
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_password_is_set(self, app_test_data):
        app_test_data.password = 'test_name'
        app = self.__create_cloud_hub_app(app_test_data)
        assert app_test_data.password == app.password

    def test_password_must_not_be_none(self, app_test_data):
        app_test_data.password = None
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_password_must_not_be_empty(self, app_test_data):
        app_test_data.password = ''
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_password_must_not_be_all_spaces(self, app_test_data):
        app_test_data.password = None
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)

    def test_password_must_not_be_whitespace(self, app_test_data):
        app_test_data.password = ' \n \t  '
        with pytest.raises(ValueError):
            self.__create_cloud_hub_app(app_test_data)


# noinspection PyShadowingNames
class CloudHubAppFunctionalityTests(object):
    """
    Tests using CloudHub API (using Mocks)
    """
    @staticmethod
    def __setup_httpretty_for_get_info(app_test_data, set_to_succeed=True, app_name=None):
        if not app_name:
            app_name = app_test_data.app_name

        if set_to_succeed:
            httpretty.register_uri(
                    httpretty.GET,
                    CloudHubApp.BASE_URL + app_name,
                    body=APPLICATION_INFO_RESPONSE_200,
                    content_type='application/json',
                    status=200
            )
        else:
            httpretty.register_uri(
                    httpretty.GET,
                    CloudHubApp.BASE_URL + app_name,
                    body=APPLICATION_INFO_RESPONSE_400,
                    content_type='text/html',
                    status=404
            )

    @staticmethod
    def __create_dummy_zipfile():
        file_1 = BytesIO(b'Some File')
        file_2 = BytesIO(b'Some Other File')
        file_3 = BytesIO(b'Yet Another File')

        with BytesIO() as memory_file:
            with zipfile.ZipFile(memory_file, 'w') as zippy:
                zippy.writestr('file_1.txt', file_1.read())
                zippy.writestr('file_2.txt', file_2.read())
                zippy.writestr('file_3.txt', file_3.read())

            memory_file.seek(0)
            return memory_file.read()

    @httpretty.activate
    def test_get_info_with_valid_application(self, app_test_data, cloudhub_app):

        # setup HTTP mock
        self.__setup_httpretty_for_get_info(app_test_data)

        app_info = cloudhub_app.get_app_info()
        assert app_test_data.app_name == app_info['domain']
        assert '3.6.0' == app_info['muleVersion']
        assert app_test_data.user_name == app_info['userName']
        assert 'lastUpdateTime' in app_info
        assert 'status' in app_info
        assert 'workers' in app_info
        assert 'filename' in app_info
        assert 'instanceSize' in app_info
        assert 'supportedVersions' in app_info
        assert 'instanceSize' in app_info

    @httpretty.activate
    def test_get_info_with_invalid_application(self, app_test_data, cloudhub_app):
        bad_app_name = app_test_data.app_name + '1'

        # setup HTTP mock
        self.__setup_httpretty_for_get_info(app_test_data, set_to_succeed=False, app_name=bad_app_name)

        cloudhub_app.name = bad_app_name
        with pytest.raises(ApplicationNotFoundError):
            cloudhub_app.get_app_info()

    @httpretty.activate
    def test_deploy_with_valid_application(self, app_test_data, cloudhub_app):

        # setup HTTP mock for get_info
        self.__setup_httpretty_for_get_info(app_test_data)

        # setup HTTP mock for the deploy (POST)
        httpretty.register_uri(
                httpretty.PUT,
                CloudHubApp.BASE_URL + app_test_data.app_name
        )

        # prepare file to deploy
        # noinspection PyShadowingBuiltins
        buffer = self.__create_dummy_zipfile()
        with BytesIO(buffer) as memory_file:
            assert zipfile.is_zipfile(memory_file)
            memory_file.seek(0)
            cloudhub_app.deploy(memory_file)

        assert 'PUT' == httpretty.last_request().method
        assert httpretty.last_request().headers['content-type'].startswith('multipart/form-data')


# noinspection SpellCheckingInspection
APPLICATION_INFO_RESPONSE_200 = """
{
  "id": "54fdd19be4b0087051e37956",
  "href": "https://cloudhub.io/api/applications/test_app",
  "domain": "test_app",
  "fullDomain": "test_app.cloudhub.io",
  "properties": {
    "test.val": "mule",
    "env": "qa",
    "super.secret.key": "****************************************************************"
  },
  "propertiesOptions": {"super.secret.key": {"secure": true}},
  "status": "STARTED",
  "workers": 1,
  "workerType": "Medium",
  "workerStatuses": [
    {
      "id": "i-282cfc25",
      "host": "54.132.170.5",
      "port": 0,
      "status": "STARTED",
      "staticIPEnabled": false
    }
  ],
  "lastUpdateTime": 1426881445619,
  "filename": "test_app-1.0.0-SNAPSHOT.zip",
  "remainingWorkerCount": 3,
  "muleVersion": "3.6.0",
  "supportedVersions": [
    "3.3.2",
    "3.4.0",
    "3.5.0",
    "3.4.1",
    "CloudHub Mule Runtime (Oct 2013)",
    "CloudHub Mule Runtime (Dec 2013)",
    "3.4.2",
    "3.5.1",
    "API Gateway 1.1.1",
    "3.5.2",
    "API Gateway 1.2.1",
    "3.4.3",
    "3.6.0",
    "API Gateway 1.3.1",
    "3.6.1"
  ],
  "instanceSize": "m3.medium",
  "tenants": 0,
  "region": "us-west-2",
  "userId": "54fe34b8e4b0d48f36b1ab70",
  "userName": "test_user",
  "vpnConfig": {
    "network": "101.0.113.0",
    "mask": 27
  },
  "hasFile": true,
  "staticIPsEnabled": false,
  "multitenanted": false,
  "persistentQueues": false,
  "monitoringAutoRestart": true,
  "encryptedPersistentQueues": false,
  "ipAddresses": [],
  "secureDataGateway": {"connected": false},
  "secureDataGatewayEnabled": false,
  "vpnEnabled": false
}
"""

# noinspection SpellCheckingInspection
APPLICATION_INFO_RESPONSE_400 = """
<!doctype html>
<html>
<head><title>CloudHub</title>
    <link rel="shortcut icon" href="/favicon.ico">
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
            color: #444;
            margin: 0;
        }

        a, a:active, a:visited {
            color: #607890;
        }

        footer {
            position: absolute;
            bottom: 0;
            right: 0;
            left: 0;
            height: 500px;
            margin-top: -50px; /* negative value of footer height */
            background: url("cloud_background.png") repeat-x scroll center bottom white;
            z-index: -1;
        }

        footer div.copyright {
            margin: 0px auto;
            text-align: center;
            font-size: 85%;
            padding: 400px 0 0 0;
            width: 980px;
        }

        header h1.logo {
            background-repeat: no-repeat;
            background-position: left top;
            background-image: url("logo-cloudhub-190x58.png");
            background-color: transparent;
            background-attachment: scroll;
            display: block;
            height: 58px;
            margin: 10px 0 30px;
            text-indent: -9999px;
            width: 190px;
        }

        .message {
            border: 1px solid #BBB;
            padding: 20px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            margin-top: 20px;
        }

        .message p {
            color: #999;
            font-size: 16px;
        }

        .message h1 {
            font-size: 20px;
        }

        .container {
            width: 980px;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>
<body>
<div class="container">
    <header><h1 class="logo">CloudHub</h1></header>
    <div class="message"><h1>Uh-oh spaghettios!</h1>

        <p>We're currently experiencing some cloud thunderstorms - we'll be right back!</p>

        <p>If you need to contact us, please email
            <a href="mailto:cloudhub-support@mulesoft.com">cloudhub-support@mulesoft.com</a>.
        </p></div>
</div>
<footer>
    <div class="copyright">
        Copyright &copy; Mulesoft, Inc. 2012. All Rights Reserved.
    </div>
</footer>
</body>
</html>
"""